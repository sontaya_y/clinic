<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    const CREATED_AT = 'stk_create_date';
    const UPDATED_AT = 'stk_update_date';

    protected $table = 'stock';
    protected $primaryKey = 'stk_id';

    public $fillable = [
        'stk_id',
        'stk_itm_id',
        'stk_balance',
        'stk_cost',
        'stk_nsp',
        'stk_create_user',
        'stk_create_date',
        'stk_update_user',
        'stk_update_date'
    ];

    public function item(){
        return $this->belongs_to('App\Item');;
    }
}
