<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    
    use Notifiable;

    protected $primaryKey = 'usr_id';

    const CREATED_AT = 'usr_create_date';
    const UPDATED_AT = 'usr_update_date';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usr_id',
        'username', 
        'password',
        'usr_create_user',
        'usr_create_date',
        'usr_update_user',
        'usr_update_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
