<?php

namespace App\Http\Controllers;

use App\Item;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     //   if ($request->search == '1') {
            $q = DB::table('item');       
            $q->select('item.itm_id'
                , 'item.itm_name'
                , 'item.itm_name2'
                ,DB::raw("case when item.itm_type = 1 then 'ขวด' 
                when item.itm_type = 2 then 'เข็ม' 
                when item.itm_type = 3 then 'ครั้ง' 
                when item.itm_type = 4 then 'ซอง' 
                when item.itm_type = 5 then 'แผง' 
                when item.itm_type = 6 then 'เม็ด' 
                when item.itm_type = 7 then 'หลอด' 
                when item.itm_type = 8 then 'อัน' 
                else '' end as itm_typeName")
                ,'stock.stk_balance'
                ,DB::raw("FORMAT(stock.stk_cost,2)  as stk_cost")
                ,DB::raw("FORMAT(stock.stk_nsp,2)  as stk_nsp")
                )
            ->join('stock', 'item.itm_id', '=', 'stock.stk_itm_id');
            if ($request->search != '1') {
                $q->where(DB::raw('1 = 0'));
            }
            if ($request->itm_name != null) {
                $q->where('itm_name','LIKE','%'.$request->itm_name.'%');
                $q->orwhere('itm_name2','LIKE','%'.$request->itm_name.'%');
            }
            if ($request->itm_type != null) {
                $q->where('itm_type','=',$request->itm_type);
            }
            if ($request->stk_balance != null) {
                if ($request->stk_balance_radio == '0') {
                    $q->where('stk_balance','>=',$request->stk_balance);
                }else{
                    $q->where('stk_balance','<=',$request->stk_balance);
                }
            }            
            $q->orderBy('itm_name','ASC');
            $items = $q->paginate(7);
            $items->appends(array('search' => '1','itm_name' => $request->itm_name))->links();
     //   }
        return view('item.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new Item();
        return view('item.create',compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,$this->rules(''));
        $item = Item::create(
            array_merge($request->all()
            ,['itm_create_user'=>Auth::user()->username
            ,'itm_update_user'=>Auth::user()->username]));
        $stock = Stock::create(array_merge($request->all()
        ,['stk_itm_id' => $item->itm_id
        ,'stk_create_user'=>Auth::user()->username
        ,'stk_update_user'=>Auth::user()->username]));
        return redirect()->route('item.show',$item)->with('success','เพิ่มข้อมูลยาสำเร็จ!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($itm_id)
    {
        $item = $this->findItem($itm_id);
        if($item == null){
            return abort(404);
        }
        else{
            return view('item.show',compact('item'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($itm_id)
    {
        $item = $this->findItem($itm_id);
        if($item == null){
            return abort(404);
        }
        return view('item.show',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $this->validate($request,$this->rules(''));

        Item::find($item->itm_id)->update($request->all());
        Stock::where('stk_itm_id', '=', $item->itm_id)->first()->update($request->all());
        return redirect()->route('item.show',$item)
                        ->with('success','Item updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return redirect()->route('item.index')->with('success','ลบข้อมูลยาสำเร็จ!');;
    }

        /**
     * Validate Request
     *
     * @param  \App\Item  $item
     * @return \validate
     */
    public function rules($id)
    {
       return [
        'itm_name' => 'required',
        'itm_type' => 'required',
        'itm_category' => 'required',
        'stk_balance' => 'required',
        'stk_cost' => 'required',
        'stk_nsp' => 'required',
       ];
    }
    public function findItem($itm_id){
        $q = DB::table('item');       
        $q->select('item.itm_id'
            ,'item.itm_name'
            ,'item.itm_name2'
            ,'item.itm_type'
            ,'item.itm_category'
            ,'item.itm_properties'
            ,'item.itm_instruction'
            ,'item.itm_before_meals'
            ,'item.itm_after_meals'
            ,'item.itm_breakfast'
            ,'item.itm_lunch'
            ,'item.itm_dinner'
            ,'item.itm_before_bed'
            ,'stock.stk_balance'
            ,'stock.stk_cost'
            ,'stock.stk_nsp' 
            )
        ->join('stock', 'item.itm_id', '=', 'stock.stk_itm_id');
        $q->where('itm_id','=',$itm_id);
        $item = $q->first();

        return $item;
    }
}
