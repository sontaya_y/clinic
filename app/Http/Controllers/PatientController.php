<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PatientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $q = Patient::query();
        
        if ($request->name != null) {
            $q->where('pat_name','LIKE','%'.$request->name.'%');
        }
        if ($request->lastname != null) {
            $q->where('pat_lastname','LIKE','%'.$request->lastname.'%');
        }
        if ($request->code != null) {
            $q->where('pat_code','LIKE','%'.$request->code.'%');
        }
        if ($request->idcard != null) {
            $q->where('pat_idcard','LIKE','%'.$request->idcard.'%');
        }
        if ($request->search != true) {
            $q->where('pat_id','=','0');
        }

        $patients = $q->orderBy('pat_code','ASC')->paginate(7);
       
        return view('patient.index',compact('patients'))->with('i',($request->input('page',1)-1)*7);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // show create form
        $patient = new Patient();
        return view('patient.create',compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Insert Patient
        $this->validate($request,$this->rules(''));

        if (isset($request->pat_birthday)) {
            $bd = explode("/",$request->pat_birthday);
            $request->merge(array('pat_birthday' => \Carbon\Carbon::createFromDate($bd[2]-543, $bd[1], $bd[0])));
        }
        

        $pat = new Patient($request->all());
        $pat->pat_create_user = Auth::user()->username;
        $pat->pat_update_user = Auth::user()->username;
        
        $pat->save();
        return redirect()->route('patient.show',$pat)->with('success','เพิ่มข้อมูลผู้ป่วยสำเร็จ!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        // Show Patient Detail 
        return view('patient.show',compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        // Show Edit Form
        $edit = true;
        return view('patient.show',compact('patient','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        // Update
        $this->validate($request,$this->rules($patient->pat_id));

        if (isset($request->pat_birthday)) {
            $bd = explode("/",$request->pat_birthday);
            $request->merge(array('pat_birthday' => \Carbon\Carbon::createFromDate($bd[2]-543, $bd[1], $bd[0])));
        }

        $patient->pat_update_user = Auth::user()->username;
        $patient->update($request->all());
        
        return redirect()->route('patient.show',$patient)->with('success','บันทึกข้อมูลผู้ป่วยสำเร็จ!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        //
        $patient->delete();
        return redirect()->route('patient.index')->with('success','ลบข้อมูลผู้ป่วยสำเร็จ!');;
    }

    /**
     * List JSON return the specified resource from storage.
     */
    public function list()
    {
        return Patient::all();
    }


    /**
     * Validate Request
     *
     * @param  \App\Patient  $patient
     * @return \validate
     */
    public function rules($id)
    {
       return [
        'pat_code'=>'required|unique:patients,pat_code,'.$id.',pat_id',
        'pat_name'=>'required',
        'pat_lastname'=>'required',
        'pat_sex'=>'required',
        'pat_birthday'=>'nullable|regex:/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/',
        'pat_age'=>'',
        'pat_idcard'=>'nullable|regex:/^[0-9]{13}$/|unique:patients,pat_idcard,'.$id.',pat_id',
        'pat_phone'=>'nullable|regex:/^[0-9]*$/|between:9,10',
        'pat_address'=>'nullable',
        'pat_allergic'=>'required',
        'pat_con_dis'=>'required',
       ];
    }
}