<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Stock;

class ItemCheckoutController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    //
    public function index()
    {
        return view('item.checkout_item');
    }

    public function checkout(Request $request)
    {
        $itm_ids = $request->id;
        $itm_values = $request->value;

        $ids = [];
        for($i = 0; $i < count($itm_ids); $i++) {
            $stock = Stock::where("stk_itm_id",$itm_ids[$i])->first();
            $stock->stk_balance -= $itm_values[$i];
            $stock->save();

            array_push($ids,$itm_ids[$i]);
        }
        return redirect()->route('print',['id'=>$ids]);
    }

    public function print(Request $request)
    {
        $items = [];
        $ids = $request->id;
        if($ids != null){
            for($i = 0; $i < count($ids); $i++) {
                array_push($items,Item::find($ids[$i]));
            }
        }    
        return view('item.print_item',compact('ids'));
    }

    public function list()
    {
        return Item::all();
    }

    public function getItem($id)
    {
        return Item::find($id);
    }

    public function stock($id)
    {
        return Stock::where("stk_itm_id",$id)->first();
    }
}
