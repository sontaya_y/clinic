<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\Medicalrecords;
use Illuminate\Support\Facades\Auth;


class MedicalrecordsController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        for ($i=0; $i < count($request->mr_name); $i++) { 
            $medical = new Medicalrecords();
            $medical->mr_pat_id = $request->mr_pat_id;
            $medical->mr_date = \Carbon\Carbon::createFromFormat("d/m/Y",$request->mr_date);
            $medical->mr_name = $request->mr_name[$i];


            //Move Uploaded File
            $file = $request->file('mr_img')[$i];
            $path = 'uploads';

            $time = \Carbon\Carbon::now()->format('YmdHis');
            $filename = $medical->mr_pat_id.$time.$i.'.'.$file->getClientOriginalExtension();
            $file->move($path,$filename);

            $medical->mr_img = $path.'/'.$filename;
            $medical->mr_create_user = Auth::user()->username;
            $medical->mr_update_user = Auth::user()->username;
            $medical->save();
        }


        $patient = Patient::find($request->mr_pat_id);
        return redirect()->route('patient.show',$patient)->with('success','เพิ่มข้อมูลผู้ป่วยสำเร็จ!')->with('tabs','history');
    }

    /**
     * Update the specified resource from storage.
     */
    public function update(Request $request)
    {
        $medical = Medicalrecords::find($request->mr_id);
        $medical->mr_name = $request->mr_name;
        $medical->save();

        $patient = Patient::find($medical->mr_pat_id);        
        return redirect()->route('patient.show',$patient)->with('success','แก้ไขชื่อประวัติผู้ป่วยสำเร็จ!')->with('tabs','history');;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $medical = Medicalrecords::find($request->delMedicalid);
        $medical->delete();

        $patient = Patient::find($request->mr_pat_id_del);        
        return redirect()->route('patient.show',$patient)->with('success','ลบข้อมูลสำเร็จ!')->with('tabs','history');;
    }

    /**
     * Validate Request
     */
    public function rules()
    {
       return [
        'mr_date'=>'required',
        'mr_pat_id'=>'required',
        'mr_name'=>'required',
        'mr_img'=>'required|max:2000'
       ];
    }

}
