<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormFacade;

class FormComponent extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register Component
        FormFacade::component('inpText', 'components.form.text', ['label','name', 'value' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpPassword', 'components.form.password', ['label','name', 'value' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpNumber', 'components.form.number', ['label','name', 'value' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpTextArea', 'components.form.textarea', ['label','name', 'value' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpDate', 'components.form.date', ['label','name', 'value' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpSelect', 'components.form.select', ['label','name', 'value' => [], 'default' => null,'col' => '12', 'attributes' => []]);
        FormFacade::component('inpCheckbox', 'components.form.checkbox', ['label','name', 'value' => null,'checked','col' => '12', 'attributes' => []]);
    } 

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
