<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicalrecords extends Model
{
    //
    const CREATED_AT = 'mr_create_date';
    const UPDATED_AT = 'mr_update_date';

    protected $primaryKey = 'mr_id';
    
    protected $fillable = [
        'mr_id',
        'mr_pat_id',
        'mr_date',
        'mr_name',
        'mr_img',
        'mr_create_user',
        'mr_update_user'
    ];
}
