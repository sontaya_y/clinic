<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    const CREATED_AT = 'itm_create_date';
    const UPDATED_AT = 'itm_update_date';

    protected $table = 'item';
    protected $primaryKey = 'itm_id';
    protected $appends = ['allname'];

    public $fillable = [
        'itm_id',
        'itm_name',
        'itm_name2',
        'itm_type',
        'itm_category',
        'itm_properties',
        'itm_instruction',
        'itm_before_meals',
        'itm_after_meals',
        'itm_breakfast',
        'itm_lunch',
        'itm_dinner',
        'itm_before_bed',
        'itm_create_user',
        'itm_create_date',
        'itm_update_user',
        'itm_update_date'
    ];

    public function stock(){
        return $this->hasOne('App\Stock','stk_itm_id');;
    }

    public function getTypeName() {
        $typeName = '';
        if ($this->itm_type == 1) {
            $typeName = 'ยาเม็ด';
        } elseif ($this->itm_type == 2) {
            $typeName = 'แคปซูล';
        }
        return $typeName;
    }

    public function getAllnameAttribute()
    {
        return $this->itm_name.','.$this->itm_name2;
    }

}