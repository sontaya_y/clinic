<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    const CREATED_AT = 'pat_create_date';
    const UPDATED_AT = 'pat_update_date';

    protected $primaryKey = 'pat_id';

    protected $appends = ['fullname'];

    protected $fillable = [
        'pat_id',
        'pat_code',
        'pat_name', 
        'pat_lastname',
        'pat_birthday',
        'pat_age',
        'pat_sex',
        'pat_phone',
        'pat_idcard',
        'pat_address',
        'pat_allergic',
        'pat_con_dis',
        'pat_create_user',
        'pat_update_user',
    ];

    /**
     * Get the Medicalrecords.
     */
    public function medicals()
    {
        return $this->hasMany('App\Medicalrecords','mr_pat_id')->orderBy('mr_date','DESC');
    }

    public function getAgeAttribute()
    {
        $now = \Carbon\Carbon::now();
        $age = \Carbon\Carbon::parse($now)->diff(\Carbon\Carbon::parse($this->pat_birthday))->format('%y');
        return  $age;
    }

    public function getBirthdayBEAttribute()
    {
        
        if ($this->pat_birthday == null) {
            return null;
        }
        $bd = \Carbon\Carbon::parse($this->pat_birthday);
        return  $bd->day."/".$bd->month."/".($bd->year+543);
    }

    public function getBirthdayADAttribute()
    {
        
        if ($this->pat_birthday == null) {
            return null;
        }
        $bd = \Carbon\Carbon::parse($this->pat_birthday);
        return  $bd->day."/".$bd->month."/".$bd->year;
    }

    public function getBirthdayAttribute()
    {
        $bd = \Carbon\Carbon::parse($this->pat_birthday)->format('d-m-Y');
        if ($this->pat_birthday == null) {
            return null;
        }
        return  $bd;
    }

    public function getFullnameAttribute()
    {
        return  $this->pat_name . ' ' . $this->pat_lastname;
    }
}
