<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('itm_id');
            $table->string('itm_name');
            $table->string('itm_name2')->nullable();
            $table->string('itm_type');
            $table->string('itm_category');
            $table->string('itm_properties');
            $table->string('itm_instruction');
            $table->boolean('itm_before_meals')->nullable();
            $table->boolean('itm_after_meals')->nullable();
            $table->boolean('itm_breakfast')->nullable();
            $table->boolean('itm_lunch')->nullable();
            $table->boolean('itm_dinner')->nullable();
            $table->boolean('itm_before_bed')->nullable();
            $table->string('itm_create_user'); 
            $table->dateTime('itm_create_date');	
            $table->string('itm_update_user');
            $table->dateTime('itm_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
