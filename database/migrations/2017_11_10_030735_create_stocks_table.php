<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->increments('stk_id');
            $table->integer('stk_itm_id');
            $table->integer('stk_balance');
            $table->double('stk_cost', 8, 2);
            $table->double('stk_nsp', 8, 2);
            $table->string('stk_create_user'); 
            $table->dateTime('stk_create_date');	
            $table->string('stk_update_user');
            $table->dateTime('stk_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
