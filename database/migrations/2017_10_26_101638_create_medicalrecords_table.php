<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicalrecords', function (Blueprint $table) {
            
            $table->increments('mr_id');
            $table->integer('mr_pat_id');
            $table->date('mr_date');            
            $table->string('mr_name');
            $table->string('mr_img');
            
            $table->string('mr_create_user'); 
            $table->dateTime('mr_create_date');	
            $table->string('mr_update_user');
            $table->dateTime('mr_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicalrecords');
    }
}
