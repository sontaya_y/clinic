<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('pat_id');
            $table->string('pat_code', 10)->unique();
            $table->string('pat_name', 40);
            $table->string('pat_lastname', 40);
            $table->date('pat_birthday')->nullable();
            $table->integer('pat_age')->nullable();;
            $table->integer('pat_sex');
            $table->string('pat_phone')->nullable();
            $table->string('pat_idcard',13)->nullable();
            $table->text('pat_address')->nullable();
            $table->text('pat_allergic');
            $table->text('pat_con_dis');
            
            $table->string('pat_create_user'); 
            $table->dateTime('pat_create_date');	
            $table->string('pat_update_user');
            $table->dateTime('pat_update_date');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
