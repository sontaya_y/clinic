<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('123123'),
            'usr_create_user' => 'admin',
            'usr_create_date' => \Carbon\Carbon::now(), 
            'usr_update_user' => 'admin', 
            'usr_update_date' => \Carbon\Carbon::now(),  
        ]);
    }
}

