<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus" lang="{{ app()->getLocale() }}">
<!--<![endif]-->

<head>
    <meta charset="utf-8">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet" href="{{ asset('css/font.css') }}">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('css/oneui.css') }}">

    @yield('css')
    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="{{ asset('css/themes/flat.min.css') }}"> -->
    <!-- END Stylesheets -->
</head>

<body>

<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed hidden-print">
            

            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            
                            <a class="h5 text-white" href="/">
                                <i class="fa fa-2x fa-stethoscope text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">Clinic</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                
                                <li>
                                    <a href="{{ route('home') }}" class="@if(Route::currentRouteName()=='home')active @endif"><i class="si si-home"></i><span class="sidebar-mini-hide">หน้าแรก</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">คลังยา</span></li>
                                <li>
                                    <a href="{{ route('item.create') }}" class="@if(Route::currentRouteName()=='item.create')active @endif"><i class="si si-plus"></i><span class="sidebar-mini-hide">เพิ่มข้อมูลยา</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('item.index') }}" class="@if(Route::currentRouteName()=='item.index'||Route::currentRouteName()=='item.show')active @endif"><i class="si si-magnifier"></i><span class="sidebar-mini-hide">ค้นหาข้อมูลยา</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('checkout') }}" class="@if(Route::currentRouteName()=='checkout')active @endif"><i class="si si-basket-loaded"></i><span class="sidebar-mini-hide">จ่ายยา</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('print') }}" class="@if(Route::currentRouteName()=='print')active @endif"><i class="si si-printer"></i><span class="sidebar-mini-hide">ปริ้นฉลากยา</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">ผู้ป่วย</span></li>
                                <li>
                                    <a href="{{ route('patient.create') }}" class="@if(Route::currentRouteName()=='patient.create')active @endif"><i class="si si-plus"></i><span class="sidebar-mini-hide">เพิ่มข้อมูลผู้ป่วย</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('patient.index') }}" class="@if(Route::currentRouteName()=='patient.index'||Route::currentRouteName()=='patient.show')active @endif"><i class="si si-magnifier"></i><span class="sidebar-mini-hide">ค้นหาข้อมูลผู้ป่วย</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">ผู้ใช้งาน</span></li>
                                <li>
                                    <a href="{{ route('user.index') }}" class="@if(Route::currentRouteName()=='user.index')active @endif"><i class="si si-users"></i><span class="sidebar-mini-hide">จัดการผู้ใช้</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full ">

                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                                <i class="fa fa-user-circle "></i>
                                {{ Auth::user()->username }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('password.request') }}">
                                        <i class="si si-key pull-right"></i>เปลี่ยนรหัสผ่าน
                                    </a>
                                    <a tabindex="-1" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="si si-logout pull-right"></i>Log out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
    
                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix ">
                <div class="pull-right">
                    <i class="fa fa-stethoscope text-primary"></i> <span class="">Clinic Management System</span>
                </div>
                <div class="pull-left">
                    
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->




    <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
    <script src="{{ asset('js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.scrollLock.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.countTo.min.js') }}"></script>
    <script src="{{ asset('js/core/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('js/core/js.cookie.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>   
    
    <script src="{{ asset('js/plugins/sweetalert2/es6-promise.auto.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    @yield('js')
    
    @if ($message = Session::get('success'))
        <script>
            swal('Success', '{{ $message }}', 'success');
        </script>
    @endif

    @if ($message2 = Session::get('error'))
        <script>
            swal('Error', '{{ $message2 }}', 'error');
        </script>
    @endif
    
</body>

</html>