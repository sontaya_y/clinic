@extends('layouts.template') 
@section('content')
<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                #{{$patient->pat_code}}
                <small>คุณ{{$patient->pat_name}} {{$patient->pat_lastname}} </small>
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Home</li>
                
                <li>
                    <a class="link-effect" href="{{ route('patient.index') }}">ค้นหาผู้ป่วย</a>
                </li>
                <li>
                    <a class="link-effect" href="{{ route('patient.show',$patient) }}">#{{$patient->pat_code}}</a>
                </li>
                @if(Route::currentRouteName()=='patient.edit')
                <li>
                    <a class="link-effect" href="">แก้ไขข้อมูล</a>
                </li>
                @endif
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="block">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs">
            <li class="active" id="tab-profile-b">
                <a href="#tab-profile">ข้อมูลผู้ป่วย</a>
            </li>
            <li class="" id="tab-history-b">
                <a href="#tab-history">ประวัติการรักษา</a>
            </li>
        </ul>
        <div class="block-content tab-content">
            <div class="tab-pane active" id="tab-profile">
                {!! Form::open(['route' => ['patient.update',$patient],'method'=>'PUT','class'=>'form-horizontal push-10-t']) !!}
                <div class="row">

                    <div class="col-sm-12">
                        {{ Form::inpText('รหัสผู้ป่วย','pat_code',$patient->pat_code,'sm-3',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::inpText('ชื่อ','pat_name',$patient->pat_name,'sm-9',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::inpText('นามสกุล','pat_lastname',$patient->pat_lastname,'sm-9',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::inpSelect('เพศ','pat_sex',['0'=>'ชาย','1'=>'หญิง'],$patient->pat_sex,'sm-4',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-5">{{ Form::inpText('วันเกิด','pat_birthday',$patient->birthdayBE,'sm-12',['disabled'=>Route::currentRouteName()!='patient.edit','placeholder'=>'วว/ดด/ปปปป','pattern'=>"[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}",'title'=>'กรุณากรอก วว/ดด/ปปปป','onchange'=>'ageCal(this)']) }}</div>
                            <div class="col-md-5">{{ Form::inpNumber('อายุ','pat_age',($patient->pat_birthday == null ? $patient->pat_age : $patient->age),'sm-6',['disabled'=>Route::currentRouteName()!='patient.edit','placeholder'=>'กรุณาระบุอายุ']) }}</div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        {{ Form::inpText('เลขบัตรประชาชน','pat_idcard',$patient->pat_idcard,'sm-7',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::inpText('เบอร์โทรศัพท์','pat_phone',$patient->pat_phone,'sm-5',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>

                    <div class="col-sm-12">
                        {{ Form::inpText('ที่อยู่','pat_address',$patient->pat_address,'sm-12',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>

                    <div class="col-sm-6">
                        {{ Form::inpText('ประวัติการแพ้ยา','pat_allergic',$patient->pat_allergic,'sm-12',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::inpText('โรคประจำตัว','pat_con_dis',$patient->pat_con_dis,'sm-12',['disabled'=>Route::currentRouteName()!='patient.edit']) }}
                    </div>
                    @if(Route::currentRouteName()=='patient.edit')
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="บันทึก"><i class="fa fa-save"></i></button>
                        <a href="{{ route('patient.show',$patient) }}" class="btn btn-default" data-toggle="tooltip" title="ยกเลิก"><i class="si si-action-undo"></i> </a>
                    </div>
                    @else
                    <div class="col-sm-6">
                        <a href="{{ route('patient.edit',$patient) }}" class="btn btn-primary" data-toggle="tooltip" title="แก้ไข"><i class="si si-pencil"></i></a>
                        <a href="javascript:del()" class="btn btn-danger" data-toggle="tooltip" title="ลบ"><i class="si si-trash"></i></a>
                    </div>
                    @endif
                </div>
                {!! Form::close() !!}
                <br>
                {!! Form::open(['method' => 'DELETE','route' => ['patient.destroy', $patient->pat_id],'style'=>'display:none','id'=>'delForm']) !!}
                {!! Form::submit('del', ['style' => 'display:none;']) !!}
                {!! Form::close() !!}
            </div>
            <div class="tab-pane" id="tab-history">
                <div class="row">
                    <div class="col-sm-3" >
                        <div>
                            <div class="block" style="">
                                    <div class="block-content block-content-full" style="padding-left: 0px;">
                                        {!! Form::open(['action' => ['MedicalrecordsController@store'],'method'=>'POST','class'=>'','files'=>'true']) !!}
                                        
                                        <div class="@if ($errors->has('mr_date')) has-error @endif">
                                            <input class="js-datepicker form-control" style='margin: 5px;' type="text" id="mr_date" name="mr_date" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::now()->format('d/m/Y') }}">
                                        </div>
                                        <div id="medicalForm">
                                            <div style="padding-top: 5px;">
                                                <input type="text" name="mr_name[]" required class="form-control" style="margin: 5px" placeholder="ชื่อไฟล์ประวัติการรักษา" mrnameInput>
                                                <input type="file" name="mr_img[]" required class="form-control" style="margin: 5px" accept="image/*" onchange="imgBrowse(this)" mrimgInput>
                                            </div>                                    
                                        </div>
                                        
                                        <input type="hidden" name="mr_pat_id" value="{{ $patient->pat_id }}">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <button type="button" class="btn btn-primary" style="width:100%;margin: 5px;" onclick="addMedecalElement()"><i class="si si-plus"></i></button>
                                            </div>
                                            <div class="col-md-8">
                                                <button type="submit" class="btn btn-primary" style="width:100%;margin: 5px;" ><i class="si si-cloud-upload"></i> อัพโหลด</button>
                                            </div>
                                        </div>
                                        
                                        {!! Form::close() !!}
                                    </div>
                                    {!! Form::open(['method' => 'delete','action' => ['MedicalrecordsController@destroy'],'style'=>'display:none','id'=>'delMedicalForm']) !!}
                                        {!! Form::hidden('delMedicalid',null, ['id' => 'delMedicalid']) !!}
                                        {!! Form::hidden('mr_pat_id_del',$patient->pat_id) !!}
                                        {!! Form::submit('', ['style' => 'display:none;']) !!}
                                    {!! Form::close() !!}
                            </div>
                            <div id="scroll">                    
                                @for ($i = 0;$i < count($patient->medicals); $i++)
                                <div class="block" style="box-shadow: 0 0 2px rgba(0, 0, 0, 0.08);margin: 20px;">
                                    <a href="javascript:imgClick('{{$i}}')">
                                        <div class="block-content block-content-full ribbon ribbon-primary ribbon-left">
                                            <div class="ribbon-box font-w600" id="imgName{{ $i }}">{{ $patient->medicals[$i]->mr_name }}</div>
                                            <div class="fade" id="imgDate{{ $i }}">{{ \Carbon\Carbon::parse($patient->medicals[$i]->mr_date)->format('d/m/Y') }}</div>
                                            <div class="text-center push-20-t push">
                                                <img src="{{ asset($patient->medicals[$i]->mr_img) }}" id="{{ $i }}" mrid="{{ $patient->medicals[$i]->mr_id }}" class="img img-responsive" />
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endfor

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9" >
                    @if(count($patient->medicals) > 0)
                        <div class="row" style="margin-bottom:20px;font-size:18px;">
                            <div class="col-md-4 text-left"><a href="javascript:imgClick('1')" class="link-effect" id="img-left"></a></div>
                            <div class="col-md-4 text-center"><span id="img-center">{{ \Carbon\Carbon::parse($patient->medicals->first()->mr_date)->format('d/m/Y') }}</span></div>
                            <div class="col-md-4 text-right"><a href="javascript:imgClick('0')" class="link-effect" id="img-right"></a></div>
                        </div>
                        <img src="{{ asset($patient->medicals->first()->mr_img) }}" id="imgshow" width="100%" class="img img-responsive" />
                    @endif
                    <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->

<!-- Apps Modal -->
<!-- Opens from the button in the header -->
<div class="modal fade" id="edit-medical-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-md modal-dialog modal-dialog-middle">
        <div class="modal-content">
            <!-- Apps Block -->
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button">
                                <i class="si si-close"></i>
                            </button>
                        </li>
                    </ul>
                    <h3 class="block-title">แก้ไขข้อมูลการรักษา</h3>
                </div>
                <div class="block-content">
                        <div class="row">
                            {!! Form::open(['action' => ['MedicalrecordsController@update'],'method'=>'PUT']) !!}
                                <div class="col-md-9">
                                    <div class="form-material">
                                        <input type="text" name="mr_name" class="form-control" id="edit_mr_name" required >
                                        <label for="">ชื่อไฟล์ประวัติการรักษา</label>
                                    </div>
                                    <input type="hidden" name="mr_id" id="edit_mr_id">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary" style="width:100%"><i class="fa fa-save"></i> บันทึก</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div>
                            <img src="#" class="img-responsive" id="edit_mr_img" alt="">
                        </div>
                    <br>
                </div>
            </div>
            <!-- END Apps Block -->
        </div>
    </div>
</div>
<!-- END Apps Modal -->
@endsection 

@section('js') 
<script type="text/javascript" <script src="{{ asset('js/core/jquery.scrollLock.min.js') }}"></script>
<script>

@if(Session::get('tabs') || $errors->has('mr_date')  || $errors->has('mr_name') || $errors->has('mr_img') || $errors->has('mr_pat_id'))
    $('#tab-profile-b').removeClass("active");
    $('#tab-profile').removeClass("active");
    $('#tab-history-b').addClass("active");
    $('#tab-history').addClass("tab-pane active");

    @if(count($patient->medicals) == 0)
        var h = 0;
    @elseif(count($patient->medicals) > 0)
        var h = $('#imgshow')[0].height - 163;
    @endif

    $('#scroll').slimScroll({
        height: h + 'px'
    });
@endif

function del(){
    swal({
        title: 'ลบข้อมูลผู้ป่วยคนนี้?',
        text: 'เมื่อลบแล้วจะไม่สามารถกู้คืนได้ คุณแน่ใจหรือไม่!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d26a5c',
        confirmButtonText: 'ใช่, ฉันต้องการลบ!',
        cancelButtonText: 'ไม่',
        html: false,
        preConfirm: function() {
            return new Promise(function (resolve) {
                setTimeout(function () {
                    resolve();
                }, 50);
            });
        }
    }).then(
        function (result) {
            $('#delForm').submit()
        }, function(dismiss) {
            
        }
    );
}

$('#tab-history-b').click(
    function(){
        @if(count($patient->medicals) == 0)
            var h = 0;
        @elseif(count($patient->medicals) > 0)
            var h = $('#imgshow')[0].height - 163;
        @endif
       $('#scroll').slimScroll({
        height: h + 'px'
    });
});

// เปลี่ยนภาพเมื่อคลิก
function imgClick(id){
    $('#imgshow')[0].src = $('#'+id)[0].src;
    imgMenu(id);
}

// เปลี่ยนเมนูด้านบน
function imgMenu(id){
    var iconBack = '<i class="fa fa-chevron-circle-left"></i> ';
    var iconNext = ' <i class="fa fa-chevron-circle-right"></i>';
    var mrid = $('#'+id)[0].getAttribute('mrid');
    var maxId = {{$i-1}};
    
    if(id == maxId && id != 0){
        $('#img-left')[0].innerHTML = "";
        $('#img-center')[0].innerHTML = $('#imgName'+id)[0].innerHTML + " [ " + $('#imgDate'+id)[0].innerHTML + " ] " + genBtnEditImg(id) + genBtnDelImg(mrid);
        $('#img-right')[0].innerHTML = $('#imgDate'+(parseInt(id)-1))[0].innerHTML + iconNext;
    }
    else if(id == 0 && maxId == 0){
        $('#img-left')[0].innerHTML = "";
        $('#img-center')[0].innerHTML = $('#imgName'+id)[0].innerHTML + " [ " + $('#imgDate'+id)[0].innerHTML + " ] " + genBtnEditImg(id) + genBtnDelImg(mrid);
        $('#img-right')[0].innerHTML = "";
    }
    else if(id == 0){
        $('#img-left')[0].innerHTML = iconBack + $('#imgDate'+(parseInt(id)+1))[0].innerHTML;
        $('#img-center')[0].innerHTML = $('#imgName'+id)[0].innerHTML + " [ " + $('#imgDate'+id)[0].innerHTML + " ] " + genBtnEditImg(id) + genBtnDelImg(mrid);
        $('#img-right')[0].innerHTML = "";
    }
    else {
        $('#img-left')[0].innerHTML = iconBack + $('#imgDate'+(parseInt(id)+1))[0].innerHTML;
        $('#img-center')[0].innerHTML = $('#imgName'+id)[0].innerHTML + " [ " + $('#imgDate'+id)[0].innerHTML + " ] " + genBtnEditImg(id) + genBtnDelImg(mrid);
        $('#img-right')[0].innerHTML = $('#imgDate'+(parseInt(id)-1))[0].innerHTML + iconNext;
    }
    $('#img-left')[0].href = "javascript:imgClick('"+ (parseInt(id)+1) +"')";
    $('#img-center')[0].href = "javascript:imgClick('"+ id +"')";
    $('#img-right')[0].href = "javascript:imgClick('"+ (parseInt(id)-1) +"')";
}

// Gen Button Delete Image
function genBtnDelImg(id){
    return '<button class="btn btn-danger btn-sm" type="button" onclick="delMedical('+id+')"><i class="fa fa-trash-o"></i></button>';
}
// Gen Button Edit Name Image
function genBtnEditImg(id){
    return '<button class="btn btn-primary btn-sm" type="button" style="margin-right: 5px;" onclick="editMedical('+id+')"><i class="fa fa-pencil"></i></button>';
}

// Edit Medical
function editMedical(id){
    $('#edit_mr_id').val($('#'+id)[0].getAttribute('mrid'));
    $('#edit_mr_name').val($('#imgName'+id)[0].innerHTML);
    $('#edit_mr_name').select();
    $('#edit_mr_img')[0].src = $('#'+id)[0].src;
    $('#edit-medical-modal').modal('show');
}

// Delete Image
function delMedical(id){
    swal({
        title: 'คุณต้องการลบประวัติการรักษา?',
        text: 'เมื่อลบแล้วจะไม่สามารถกู้คืนได้ คุณแน่ใจหรือไม่!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d26a5c',
        confirmButtonText: 'ใช่, ฉันต้องการลบ!',
        cancelButtonText: 'ไม่',
        html: false,
        preConfirm: function() {
            return new Promise(function (resolve) {
                setTimeout(function () {
                    resolve();
                }, 50);
            });
        }
    }).then(
        function (result) {
            $('#delMedicalid').val(id);
            $('#delMedicalForm').submit();
        }, function(dismiss) {
            
        }
    );

}

// Cal Age
function ageCal(inp){
    var birthday = $(inp).val();
    console.log(birthday);
    if(/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(birthday)){
        var now = new Date();
        $('#pat_age').val(now.getFullYear() - (birthday.split('/')[2]-543));
        console.log(now.getFullYear() - (birthday.split('/')[2]-543));
    }
}

// Add input Medecal in Form
function addMedecalElement(){
                                                                         
    var medicalElmt = "";
    medicalElmt = '<div style="padding-top: 5px;" elmt>'
                +   '<div class="block-options"><button type="button" class="btn btn-rounded btn-danger " onclick="delMedecalElement(this)"><i class="si si-close"></i></button></div>'
                +   '<input type="text" name="mr_name[]" required class="form-control" style="margin: 5px" placeholder="ชื่อไฟล์ประวัติการรักษา" mrnameInput>'
                +   '<input type="file" name="mr_img[]" required class="form-control" style="margin: 5px" accept="image/*" onchange="imgBrowse(this)" mrimgInput>'
                +  '</div>';
    
    $('#medicalForm').append(medicalElmt);

}

// remove input Medecal in Form
function delMedecalElement(elmt){
    $( elmt ).parents('div[elmt]').remove();
}

// Event :: Borwse File >> mr_name = filename >> mr_name.select()
function imgBrowse(elmt) {
    $(elmt).prev().val($(elmt).val().split('\\').pop().split('\.').reverse().pop());
    $(elmt).prev().select();
};


@if(count($patient->medicals) > 0)
    imgMenu(0);
@endif       

// Auto Focus mrname -> ( Modal Show )
$('#edit-medical-modal').on('shown.bs.modal', function (e) {
  $('#edit_mr_name').select();
})
</script>
@endsection