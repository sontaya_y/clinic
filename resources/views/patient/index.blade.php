@extends('layouts.template')
@section('content')
<!-- Page Content -->
<div class="content">

    <div class="block block-bordered">
        <div class="block-header">
            <h2 class="block-title">ค้นหาผู้ป่วย</h2>
        </div>
        <div class="block-content">
            {!! Form::open(['route' => 'patient.index','method'=>'GET','class'=>'form-inline push-10-t']) !!}

                <div class="row">
                    <div class="col-md-3"> {{ Form::inpText('ชื่อ','name',Request::get('name'),'md-12',['placeholder'=>'กรุณาระบุชื่อผู้ป่วย','autofocus'=>'','style'=>'width:100%']) }} </div>
                    <div class="col-md-3"> {{ Form::inpText('นามสกุล','lastname',Request::get('lastname'),'md-12',['placeholder'=>'กรุณาระบุนามสกุลผู้ป่วย','style'=>'width:100%']) }}</div>
                    <div class="col-md-2"> {{ Form::inpText('รหัสผู้ป่วย','code',Request::get('code'),'md-12',['placeholder'=>'กรุณาระบุรหัสผู้ป่วย','style'=>'width:100%']) }}</div>
                    <div class="col-md-3"> {{ Form::inpText('เลขบัตรประชาชน','idcard',Request::get('idcard'),'md-12',['placeholder'=>'ระบุเลขบัตรประชาชน','style'=>'width:100%']) }}</div>
                    <div class="col-md-1">
                        <input type="hidden" name="search" value="true">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="ค้นหา"><i class="fa fa-search"></i></button>
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
    <div class="block block-bordered">
        <div class="block-header">
            <h2 class="block-title">รายชื่อผู้ป่วย</h2>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;"></th>
                        <th>รหัสผู้ป่วย</th>
                        <th>ชื่อ - นามสกุล</th>
                        <th>เลขบัตรประชาชน</th>
                        <th>วันเกิด</th>
                        <th>อายุ</th>
                        <th>โรคประจำตัว</th>
                        <th>ประวัติการแพ้ยา</th>
                    </tr>
                </thead>
                @foreach ($patients as $key => $patient)
                <tr>
                    <td>
                        <a href="{{ route('patient.show',$patient->pat_id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="ดูข้อมูลผู้ป่วย"><i class="fa fa-address-card-o"></i></a>  
                    </td>
                    <td style="vertical-align: middle;">{{ $patient->pat_code }}</td>
                    <td style="vertical-align: middle;">{{ $patient->pat_name }} {{ $patient->pat_lastname }}</td>
                    <td style="vertical-align: middle;">{{ $patient->pat_idcard }}</td>
                    <td style="vertical-align: middle;">{{ $patient->birthdayBE }}</td>
                    <td style="vertical-align: middle;">{{ ($patient->pat_birthday == null ? $patient->pat_age : $patient->age) }} ปี </td>
                    <td style="vertical-align: middle;">{{ $patient->pat_con_dis }}</td>
                    <td style="vertical-align: middle;">{{ $patient->pat_allergic }}</td>
                </tr>
                @endforeach
            </table>
            {{ $patients->appends(Request::input())->links() }}
        </div>
    </div>

</div>
<!-- END Page Content -->
@endsection