@extends('layouts.template') 
@section('content')
<!-- Page Content -->
<div class="content">
    <!-- Start Floating Labels -->
    <div class="block block-bordered">
        <div class="block-header">
            <h2 class="block-title">เพิ่มประวัติผู้ป่วย</h2>
        </div>
        <div class="block-content">
            {!! Form::open(['route' => 'patient.store','method'=>'POST','class'=>'form-horizontal push-10-t']) !!}
            <div class="row">

                <div class="col-sm-12">
                    {{ Form::inpText('รหัสผู้ป่วย','pat_code','','sm-3',['placeholder'=>'กรุณาระบุรหัสผู้ป่วย','autofocus'=>'']) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::inpText('ชื่อ','pat_name','','sm-9',['placeholder'=>'กรุณาระบุชื่อผู้ป่วย']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::inpText('นามสกุล','pat_lastname','','sm-9',['placeholder'=>'กรุณาระบุนามสกุลผู้ป่วย']) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::inpSelect('เพศ','pat_sex',['0'=>'ชาย','1'=>'หญิง'],null,'sm-4',['placeholder'=>'เลือก']) }}
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-md-5">{{ Form::inpText('วันเกิด','pat_birthday','','sm-12',['placeholder'=>'วว/ดด/ปปปป','pattern'=>"[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}",'title'=>'กรุณากรอก วว/ดด/ปปปป','onchange'=>'ageCal(this)']) }}</div>
                        <div class="col-md-5">{{ Form::inpNumber('อายุ','pat_age','','sm-6',['placeholder'=>'กรุณาระบุอายุ']) }}</div>
                    </div>
                </div>

                <div class="col-sm-6">
                    {{ Form::inpText('เลขบัตรประชาชน','pat_idcard','','sm-7',['placeholder'=>'กรุณาระบุเลขบัตรประชาชนผู้ป่วย']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::inpText('เบอร์โทรศัพท์','pat_phone','','sm-5',['placeholder'=>'กรุณาระบุเบอร์โทรศัพท์']) }}
                </div>

                <div class="col-sm-12">
                    {{ Form::inpText('ที่อยู่','pat_address','','sm-12',['placeholder'=>'กรุณาระบุที่อยู่']) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::inpText('ประวัติการแพ้ยา','pat_allergic','','sm-12',['placeholder'=>'กรุณาระบุประวัติการแพ้ยา']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::inpText('โรคประจำตัว','pat_con_dis','','sm-12',['placeholder'=>'กรุณาระบุโรคประจำตัว']) }}
                </div>

                <div class="col-sm-6">
                    {{ Form::submit('เพิ่ม',['class'=>'btn btn-minw btn-primary']) }}
                </div>

            </div>
            {!! Form::close() !!}
            <br>
            
        </div>
    </div>
    <!-- END Floating Labels -->
</div>
<!-- END Page Content -->
@endsection

@section('js')
<script>
// Cal Age
function ageCal(inp){
    var birthday = $(inp).val();
    if(/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(birthday)){
        var now = new Date();
        $('#pat_age').val(now.getFullYear() - (birthday.split('/')[2]-543));
    }
}
</script>
@endsection