@extends('layouts.template')
@section('css')
    <link rel="stylesheet" href="{{ asset('js/plugins/easy-autocomplete/easy-autocomplete.min.css') }}">
    <style>
        @media print
        {
            .page{
            page-break-after: right;
            }
            .page-auto{
            page-break-inside: avoid;
            }
        }
    </style>
@endsection
@section('content')
<div class="content">
    {{--  Top  --}}
    <div class="row">
        <div class="col-md-6">
            <div class="block block-bordered">
                <div class="block-header">
                    <h2 class="block-title">เลือกยา</h2>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::inpText('ชื่อยา','itemname',null,'md-12',['placeholder'=>'กรุณาระบุชื่อยา','autofocus'=>'','style'=>'width:100%;padding-left: 0;padding-right: 0;border: 0;border-radius: 0;background-color: transparent;-webkit-box-shadow: 0 1px 0 #e6e6e6;box-shadow: 0 1px 0 #e6e6e6;']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5"></div>
    </div>
    {{--  End Top  --}}
    {{--  Table  --}}
    <div class="block block-bordered ">
        <div class="block-header">
            <h2 class="block-title">รายการจ่ายยา</h2>
        </div>
        <div class="block-content" style="padding-bottom: 60px;">
            {!! Form::open(['action' => ['ItemCheckoutcontroller@checkout'],'method'=>'POST','onsubmit'=>'return checkout()']) !!}
            <table class="table table-bordered" id="cart">
                <thead>
                    <tr>
                        <th>รายการ</th>
                        <th class="text-right" style="width:150px;">จำนวน</th>
                        <th class="text-right" style="width:150px;">ราคา/หน่วย</th>
                        <th class="text-right" style="width:150px;">ราคารวม</th>
                        <th style="width:50px;"></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr class="bg-gray-lighter">
                        <th colspan="3" class="text-right">ทั้งหมด <span id="itemsum">0</span> รายการ เป็นจำนวนเงิน</th>
                        <th class="text-right"><span id="pricesum">0.00</span></th>
                        <th>บาท</th>
                    </tr>
                </tfoot>
            </table>
            
            <div style="float:right">
                <button class="btn btn-primary" type="submit"><i class="fa fa-shopping-cart"></i> จ่ายยาและปริ้นฉลาก</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--  End Table  --}}
</div>

@endsection

@section('js') 
{{--  https://goodies.pixabay.com/jquery/auto-complete/demo.html  --}}
<script type="text/javascript" <script src="{{ asset('js/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js') }}"></script>

{{--  Auto complete  --}}
<script>
    // Auto complete 
    var options = {                                                                                                    
        url: "{{ route('itemlist') }}",
        getValue: "allname",
        requestDelay: 250,
        template: {
            type: "custom",
            method: function(value, item) {
                return item.itm_name + " - <small class='text-muted'>" + item.itm_name2 +"</small>";
            }
        },
        list: {	
            match: {
            enabled: true
            },
            onChooseEvent:function(){
                if($('#item'+$("#itemname").getSelectedItemData().itm_id).length == 0){
                    add();
                }
                else {
                    $("#itemname").val('');
                }
            }
        },
        theme: "square"
    };
    $("#itemname").easyAutocomplete(options); 
 
    // add
    function add(){
        var item = $("#itemname").getSelectedItemData();
        $.get( "{{ route('itemstock',null) }}/" + item.itm_id, function( stock ) {
            var newRow = "";
            newRow  = '<tr id="item'+ item.itm_id +'">'
                    +       '<td style="vertical-align: middle">'+ item.itm_name +'<input type="hidden" name="id[]" value="'+ item.itm_id +'"></td>'
                    +       '<td class="text-right" style="vertical-align: middle">'
                    +           '<input type="number" step="any" name="value[]" id="value'+ item.itm_id +'" class="text-right form-control" min="1" onchange="sum()" value="1" max="'+stock.stk_balance+'" required>'
                    +       '</td>'
                    +       '<td class="text-right" style="vertical-align: middle">'
                    +           '<input type="number" step="any" name="price[]" id="price'+ item.itm_id +'" class="text-right form-control" min="0" onchange="sum()" value="'+stock.stk_nsp+'">'
                    +       '</td>'
                    +       '<td class="text-right" style="vertical-align: middle">'
                    +           '<input type="number" step="any" name="total[]" id="total'+ item.itm_id +'" class="text-right form-control" min="0" value="'+stock.stk_nsp+'" readonly>'
                    +       '</td>'
                    +       '<th style="width: 50px;">'
                    +           '<button class="btn btn-danger" onclick="remove(' + item.itm_id +')"><i class="fa fa-close"></i></button>'
                    +       '</th>'
                    + '</tr>';
            $('#cart').append(newRow);
            sum();
        });
        $("#itemname").val('');
    } 

    // Remove Row
    function remove(id){
        $("#item"+id).remove();
        sum();
    }

    // Checkout Before Submit
    function checkout(){
        if($('#cart')[0]['children'][1].childElementCount != 0){
            return true
        }
        else {
            swal('ผิดพลาด!', 'กรุณาเพิ่มยา', 'error');
            return false;
        }
    }

    //Sum price
    function sum(){
        var pricesum = 0;
        if($('#cart')[0]['children'][1].childElementCount != 0){
            for(var i = 0; i < $('#cart')[0]['children'][1].childElementCount;i++){
                itm_id =  $('#cart')[0]['children'][1]['children'][i].id.split("item")[1];
                $('#total'+itm_id).val( $('#value'+itm_id).val() * $('#price'+itm_id).val());
                pricesum += parseFloat($('#total'+itm_id).val());
            }
            $('#pricesum')[0].innerHTML = parseFloat(pricesum).toFixed(2);
            $('#itemsum')[0].innerHTML = $('#cart')[0]['children'][1].childElementCount;
        }
        else {
            $('#pricesum')[0].innerHTML = parseFloat(0).toFixed(2);
            $('#itemsum')[0].innerHTML = 0;
        }
    }
</script>

@endsection