@extends('layouts.template') 
@section('content')
<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                #{{$item->itm_name}}
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Home</li>
                
                <li>
                    <a class="link-effect" href="{{ route('item.index') }}">ค้นหายา</a>
                </li>
                <li>
                    <a class="link-effect" href="{{ route('item.show',$item->itm_id) }}">#{{$item->itm_name}}</a>
                </li>
                @if(Route::currentRouteName()=='item.edit')
                <li>
                    <a class="link-effect" href="">แก้ไขข้อมูล</a>
                </li>
                @endif
            </ol>
        </div>
    </div>
</div>
<!-- Page Content -->
<div class="content">
    <!-- Start Floating Labels -->
    <div class="block">
        <div class="block-header">
            <h2 class="block-title">เพิ่มข้อมูลยา</h2>
        </div>
        <div class="block-content">
            {!! Form::open(['route' => ['item.update',$item->itm_id],'method'=>'PATCH','class'=>'form-horizontal push-10-t']) !!}
            <div class="row">

                <div class="col-sm-6">
                    {{ Form::inpText('ชื่อยา','itm_name',$item->itm_name,'sm-12',['placeholder'=>'กรุณาระบุชื่อยา','disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::inpSelect('หน่วย','itm_type',['1'=>'ขวด','2'=>'เข็ม','3'=>'ครั้ง','4'=>'ซอง','5'=>'แผง','6'=>'เม็ด','7'=>'หลอด','8'=>'อัน']
                    ,$item->itm_type,'sm-9',['placeholder'=>'กรุณาระบุหน่วย','disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::inpSelect('ประเภท','itm_category',['1'=>'ยาใช้ภายนอก','2'=>'ยาใช้ภายใน','3'=>'หัตถการ','4'=>'อุปกรณ์การแพทย์'],$item->itm_category,'sm-9',['placeholder'=>'กรุณาระบุประเภทยา','disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpText('ชื่อเรียกยาเพิ่มเติม','itm_name2',$item->itm_name2,'sm-6',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpTextArea('สรรพคุณ','itm_properties',$item->itm_properties,'sm-12',['placeholder'=>'กรุณาระบุสรรพคุณ','rows'=>'5','disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpTextArea('วิธีใช้','itm_instruction',$item->itm_instruction,'sm-12',['placeholder'=>'กรุณาระบุวิธีใช้','rows'=>'5','disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('ก่อนอาหาร','itm_before_meals', 1,  $item->itm_before_meals === 1 ? true : false ,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('หลังอาหาร','itm_after_meals', 1, $item->itm_after_meals === 1 ? true : false,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('เช้า','itm_breakfast', 1, $item->itm_breakfast === 1 ? true : false,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('กลางวัน','itm_lunch', 1, $item->itm_lunch === 1 ? true : false,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('เย็น','itm_dinner', 1, $item->itm_dinner === 1 ? true : false,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('ก่อนนอน','itm_before_bed', 1, $item->itm_before_bed === 1 ? true : false,'sm-12',['disabled'=>Route::currentRouteName()!='item.edit']) }}
                </div>  
                <div class="col-sm-4">
                    {{ Form::inpNumber('จำนวนที่เหลืออยู่','stk_balance',$item->stk_balance,'sm-12',['min'=>0,'disabled'=>Route::currentRouteName()!='item.edit','style'=>'text-align:right;']) }}
                </div>
                <div class="col-sm-4">
                    {{ Form::inpNumber('ราคาต้นทุน','stk_cost',$item->stk_cost,'sm-12',['min'=>0,'disabled'=>Route::currentRouteName()!='item.edit','style'=>'text-align:right;']) }}
                </div>
                <div class="col-sm-4">
                    {{ Form::inpNumber('ราคาขาย','stk_nsp',$item->stk_nsp,'sm-12',['min'=>0,'disabled'=>Route::currentRouteName()!='item.edit','style'=>'text-align:right;']) }}
                </div>
                @if(Route::currentRouteName()=='item.edit')
                <div class="col-sm-6">
                    {{ Form::submit('บันทึก',['class'=>'btn btn-minw btn-primary']) }}          
                    <a href="{{ route('item.show',$item->itm_id) }}" class="btn btn-default" data-toggle="tooltip" title="ยกเลิก"><i class="si si-action-undo"></i> </a>
                </div>
                @else
                <div class="col-sm-6">
                    <a href="{{ route('item.edit',$item->itm_id) }}" class="btn btn-primary" data-toggle="tooltip" title="แก้ไข"><i class="si si-pencil"></i></a>
                    <a href="javascript:del()" class="btn btn-danger" data-toggle="tooltip" title="ลบ"><i class="si si-trash"></i></a>
                </div>
                @endif                
            </div>
            {!! Form::close() !!}
            <br>
            {!! Form::open(['method' => 'DELETE','route' => ['item.destroy', $item->itm_id],'style'=>'display:none','id'=>'delForm']) !!}
            {!! Form::submit('del', ['style' => 'display:none;']) !!}
            {!! Form::close() !!}
            <br>
        </div>
    </div>
    <!-- END Floating Labels -->
</div>
<!-- END Page Content -->
@endsection
@section('js')
<script>
    function del(){
        swal({
            title: 'ลบช้อมูลยานี้?',
            text: 'เมื่อลบแล้วจะไม่สามารถกู้คืนได้ คุณแน่ใจหรือไม่!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'ใช่, ฉันต้องการลบ!',
            cancelButtonText: 'ไม่',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(
            function (result) {
                $('#delForm').submit()
            }, function(dismiss) {
                
            }
        );
    }
</script>
@endsection