@extends('layouts.template')
@section('css')
    <link rel="stylesheet" href="{{ asset('js/plugins/easy-autocomplete/easy-autocomplete.min.css') }}">
    <style>
        @media print
        {
            .page{
                page-break-after: right;
            }
            .page-auto{
                page-break-inside: avoid;
            }
            body {
                font-family: "Angsana New";
                font-size: 18px;
            }
            span{
                font-weight: bold;
            }    
            div {
                outline: 0px solid;
                text-overflow: ellipsis;
                line-height: 0.6cm;
                  /*overflow: hidden;*/
            }
            .stickers {
                width: 7.7cm;
                height: 4.4cm;
            }
            .head {
                height: 0.6cm;
                white-space: nowrap;
                margin-top: 0.8cm;
                float: left;
            }
            .name {
                width: 3.8cm;
            }
            .age {
                width: 1.5cm;
            }
            .date {
                width: 2.3cm;
            }
            .itemname {
                height: 0.5cm;
                width: 100%;
                float: left;
            }
            .properties {
                height: 1.2cm;
                width: 100%;
                float: left;
            }
            .instruction {
                height: 0.6cm;
                width: 100%;
                float: left;
            }
            .meals {
                height: 0.6cm;
                width: 100%;
                float: left;
                text-align: center;
            } 
        }
        @page{
            margin-top: 0.3cm;
            margin-bottom: 0.1cm;
            margin-right: 0.2cm;
            margin-left: 0.4cm;
         }

    </style>
@endsection
@section('content')
<div class="content">
    {{--  Top  --}}
    <div class="row">
        <div class="col-md-6">
            <div class="block block-bordered">
                <div class="block-header">
                    <h2 class="block-title">เลือกยา</h2>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::inpText('ชื่อยา','itemname',null,'md-12',['placeholder'=>'กรุณาระบุชื่อยา','autofocus'=>'','style'=>'width:100%;padding-left: 0;padding-right: 0;border: 0;border-radius: 0;background-color: transparent;-webkit-box-shadow: 0 1px 0 #e6e6e6;box-shadow: 0 1px 0 #e6e6e6;']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block block-bordered">
                <div class="block-header">
                    <h2 class="block-title">ข้อมูลผู้รับยา</h2>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::inpText('ชื่อ-นามสกุล','pat_name_print',null,'md-12',['placeholder'=>'กรุณาระบุชื่อผู้ป่วย','autofocus'=>'','style'=>'width:100%;padding-left: 0;padding-right: 0;border: 0;border-radius: 0;background-color: transparent;-webkit-box-shadow: 0 1px 0 #e6e6e6;box-shadow: 0 1px 0 #e6e6e6;']) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::inpText('วันที่','date','','md-12',['style'=>'width:100%']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--  End Top  --}}
    {{--  Table  --}}
    <div class="block block-bordered ">
        <div class="block-header">
            <h2 class="block-title">รายการจ่ายยา</h2>
        </div>
        <div class="block-content" style="padding-bottom: 60px;">
            <table class="table table-bordered table-hover" id="cart">
                <thead>
                    <tr>
                        <th class="text-center">รายการ</th>
                        <th class="text-center" style="width:350px">สรรพคุณ</th>
                        <th class="text-center" style="width:350px">วิธีใช้</th>
                        <th class="text-center" style="width:110px">ก่อนอาหาร</th>
                        <th class="text-center" style="width:110px">หลังอาหาร</th>
                        <th class="text-center" style="width:80px">เช้า</th>
                        <th class="text-center" style="width:80px">กลางวัน</th>
                        <th class="text-center" style="width:80px">เย็น</th>
                        <th class="text-center" style="width:85px">ก่อนนอน</th>
                        <th class="text-center" style="width:50px"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <div style="float:right">
                <button class="btn btn-primary" onclick="printTable()"><i class="si si-printer"></i> พิมพ์</button>
            </div>
        </div>
    </div>
    {{--  End Table  --}}
</div>
@endsection

@section('js') 

{{--  Print  --}}
<div class="visible-print-block" id="printPaper"></div>

{{--  https://goodies.pixabay.com/jquery/auto-complete/demo.html  --}}
<script type="text/javascript" <script src="{{ asset('js/plugins/easy-autocomplete/jquery.easy-autocomplete.min.js') }}"></script>

{{--  Auto complete  --}}
<script type="text/javascript">
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear()+543;
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    var printDate = dd+'/'+mm+'/'+yyyy;
    $('#date').val(printDate);

    var patprint = null;
    var options = {                                                                                                    
        url: "{{ route('itemlist') }}",
        getValue: "allname",
        requestDelay: 250,
        template: {
            type: "custom",
            method: function(value, item) {
                return item.itm_name + " - <small class='text-muted'>" + item.itm_name2 +"</small>";
            }
        },
        list: {	
            match: {
            enabled: true
            },
            onChooseEvent:function(){
                if($('#item'+$("#itemname").getSelectedItemData().itm_id).length == 0){
                    add();
                }
                else {
                    $("#itemname").val('');
                }
            }
        },
        theme: "square"
    };
    $("#itemname").easyAutocomplete(options); 
    
    var options2 = {                                                                                                    
        url: "{{ route('patientlist') }}",
        getValue: "fullname",
        requestDelay: 250,
        list: {	
            match: {
            enabled: true
            },
            onChooseEvent:function(){
                patprint = $('#pat_name_print').getSelectedItemData();
            },
            onLoadEvent:function(){
                patprint = null;
            }
        },
        theme: "square"
    };
    $("#pat_name_print").easyAutocomplete(options2); 

    // add to table
    function add(item = null){
        if(item == null){
            item = $("#itemname").getSelectedItemData();
        }
        
        item.itm_before_meals   = (item.itm_before_meals === 1) ? "checked" : "";
        item.itm_after_meals    = (item.itm_after_meals === 1) ? "checked" : "";
        item.itm_breakfast      = (item.itm_breakfast === 1) ? "checked" : "";
        item.itm_lunch          = (item.itm_lunch === 1) ? "checked" : "";
        item.itm_dinner         = (item.itm_dinner === 1) ? "checked" : "";
        item.itm_before_bed     = (item.itm_before_bed === 1) ? "checked" : "";
        
        var newRow = "";
        newRow  = '<tr id="item'+ item.itm_id +'">'
                +   '<td style="vertical-align: middle" id="itm_name'+ item.itm_id +'">' + item.itm_name + '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<textarea class="form-control" id="itm_properties'+ item.itm_id +'" cols="30" rows="2">' + item.itm_properties + '</textarea>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<textarea class="form-control" id="itm_instruction'+ item.itm_id +'" cols="30" rows="2">' + item.itm_instruction + '</textarea>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_before_meals'+ item.itm_id +'" '+ item.itm_before_meals +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_after_meals'+ item.itm_id +'" '+ item.itm_after_meals +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_breakfast'+ item.itm_id +'" '+ item.itm_breakfast +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_lunch'+ item.itm_id +'" '+ item.itm_lunch +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_dinner'+ item.itm_id +'" '+ item.itm_dinner +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<label class="css-input css-checkbox css-checkbox-primary">'
                +           '<input type="checkbox" id="itm_before_bed'+ item.itm_id +'" '+ item.itm_before_bed +'><span></span>'
                +       '</label>'
                +   '</td>'
                +   '<td class="text-center" style="vertical-align: middle">'
                +       '<button class="btn btn-danger" onclick="remove(' + item.itm_id +')"><i class="fa fa-close"></i></button>'    
                +   '</td>'
                + '</tr>';

        $('#cart').append(newRow);
        $("#itemname").val('');
    } 

    // Remove Row
    function remove(id){
        $("#item"+id).remove();
    }

    // Print
    function printTable(){

        var printDetial = "";
        var itm_id = 0;
        if($('#cart')[0]['children'][1].childElementCount != 0){

            if(patprint != null){

                // Clean printPaper
                $('#printPaper').empty();
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!

                var yyyy = today.getFullYear()+543;
                if(dd<10){
                    dd='0'+dd;
                } 
                if(mm<10){
                    mm='0'+mm;
                } 
                var printDate = dd+'/'+mm+'/'+yyyy;
                for(var i = 0; i < $('#cart')[0]['children'][1].childElementCount;i++){
                    itm_id =  $('#cart')[0]['children'][1]['children'][i].id.split("item")[1];

                    let itm_before_meals    = ( $('#itm_before_meals'+itm_id)[0].checked == true ) ? 'ก่อนอาหาร' : '';
                    let itm_after_meals     = ( $('#itm_after_meals'+itm_id)[0].checked == true ) ? 'หลังอาหาร' : '';

                    let itm_breakfast       = ( $('#itm_breakfast'+itm_id)[0].checked == true ) ? 'เช้า' : '';
                    let itm_lunch           = ( $('#itm_lunch'+itm_id)[0].checked == true ) ? 'กลางวัน' : '';
                    let itm_dinner          = ( $('#itm_dinner'+itm_id)[0].checked == true ) ? 'เย็น' : '';
                    let itm_before_bed      = ( $('#itm_before_bed'+itm_id)[0].checked == true ) ? 'ก่อนนอน' : '';
                     
                    printDetial = '<div class = "stickers page">'+
                            '<div class="name head"><span>ชื่อ : </span>'+ $('#pat_name_print').val() +'</div>'+
                            '<div class="age head"><span>อายุ : </span>'+ patprint.pat_age +' ปี</div>'+
                            '<div class="date head"><span>วันที่ : </span>'+ printDate +'</div>'+
                            '<div class="itemname"><span>ชื่อยา : </span>'+ $('#itm_name'+itm_id).html()+'</div>'+
                            '<div class="instruction"><span>วิธีใช้ : </span>'+ $('#itm_instruction'+itm_id).val().replace(/\r\n/g, '<br>').replace(/[\r\n]/g, '<br>').replace(/ /g, '&nbsp;') +'</div>'+
                            '<div class="meals">'+ itm_before_meals + ' ' + itm_after_meals +' '+ itm_breakfast +' '+ itm_lunch +' '+ itm_dinner +' '+ itm_before_bed +'</div>'+
                            '<div class="properties"><span>สรรพคุณ : </span>'+ $('#itm_properties'+itm_id).val().replace(/\r\n/g, '<br>').replace(/[\r\n]/g, '<br>') +'</div>'+
                            '</div>';
                    $('#printPaper').append(printDetial);
                }

                // เรียก Print ของ Browser
                setTimeout(function(){
                    window.print();
                },200);
            }
            else {
                swal('ผิดพลาด!', 'กรุณาระบุชื่อผู้รับยา', 'error');
            }
        }
        else {
            swal('ผิดพลาด!', 'กรุณาเลือกยาที่จะพิมพ์', 'error');
        }
    }

    var itm = null;
    @if($ids != null && count($ids))
        @foreach ($ids as $key => $item)
            $.get("{{ route('getitem',$item) }}",function(data){
                add(data);
            });
        @endforeach
    
    @endif

</script>

@endsection