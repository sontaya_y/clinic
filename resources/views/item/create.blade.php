@extends('layouts.template') 
@section('content')
<!-- Page Content -->
<div class="content">
    <!-- Start Floating Labels -->
    <div class="block">
        <div class="block-header">
            <h2 class="block-title">เพิ่มข้อมูลยา</h2>
        </div>
        <div class="block-content">
            {!! Form::open(['route' => 'item.store','method'=>'POST','class'=>'form-horizontal push-10-t']) !!}
            <div class="row">

                <div class="col-sm-6">
                    {{ Form::inpText('ชื่อยา','itm_name','','sm-12',['placeholder'=>'กรุณาระบุชื่อยา']) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::inpSelect('หน่วย','itm_type',['1'=>'ขวด','2'=>'เข็ม','3'=>'ครั้ง','4'=>'ซอง','5'=>'แผง','6'=>'เม็ด','7'=>'หลอด','8'=>'อัน'],null,'sm-9',['placeholder'=>'กรุณาระบุหน่วย']) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::inpSelect('ประเภท','itm_category',['1'=>'ยาใช้ภายนอก','2'=>'ยาใช้ภายใน','3'=>'หัตถการ','4'=>'อุปกรณ์การแพทย์']
                    ,null,'sm-9',['placeholder'=>'กรุณาระบุประเภทยา']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpText('ชื่อเรียกยาเพิ่มเติม','itm_name2','','sm-6',[]) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpTextArea('สรรพคุณ','itm_properties','','sm-12',['placeholder'=>'กรุณาระบุสรรพคุณ','rows'=>'5']) }}
                </div>
                <div class="col-sm-12">
                    {{ Form::inpTextArea('วิธีใช้','itm_instruction','','sm-12',['placeholder'=>'กรุณาระบุวิธีใช้','rows'=>'5']) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('ก่อนอาหาร','itm_before_meals', 1, false,'sm-12') }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('หลังอาหาร','itm_after_meals', 1, false,'sm-12') }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('เช้า','itm_breakfast', 1, false,'sm-12') }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('กลางวัน','itm_lunch', 1, false,'sm-12') }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('เย็น','itm_dinner', 1, false,'sm-12') }}
                </div>
                <div class="col-sm-2">
                    {{ Form::inpCheckbox('ก่อนนอน','itm_before_bed', 1, false,'sm-12') }}
                </div>                                                                
                <div class="col-sm-4">
                    {{ Form::inpNumber('จำนวนที่เหลืออยู่','stk_balance','0','sm-12',['min'=>0,'style'=>'text-align:right;']) }}
                </div>
                <div class="col-sm-4">
                    {{ Form::inpNumber('ราคาต้นทุน','stk_cost','0','sm-12',['min'=>0,'style'=>'text-align:right;']) }}
                </div>
                <div class="col-sm-4">
                    {{ Form::inpNumber('ราคาขาย','stk_nsp','0','sm-12',['min'=>0,'style'=>'text-align:right;']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::submit('บันทึก',['class'=>'btn btn-minw btn-primary']) }}
                </div>

            </div>
            {!! Form::close() !!}
            <br>
            <br>
        </div>
    </div>
    <!-- END Floating Labels -->
</div>
<!-- END Page Content -->
@endsection