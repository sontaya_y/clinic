@extends('layouts.template') 
@section('content')
<!-- Page Content -->
<div class="content">
    <!-- Start Floating Labels -->
    <div class="block">
        <div class="block-header">
            <h2 class="block-title">ค้นหาข้อมูลยา</h2>
        </div>
        <div class="block-content">
            {!! Form::open(['route' => 'item.index','method'=>'GET','class'=>'form-horizontal push-10-t']) !!}
            <div class="row">
                {{ Form::hidden('search', '1') }}
                <div class="col-sm-4">
                    {{ Form::inpText('ชื่อยา','itm_name','','sm-12',['placeholder'=>'กรุณาระบุชื่อยา']) }}
                </div>
                <div class="col-sm-3">
                    {{ Form::inpSelect('หน่วย','itm_type',['1'=>'ขวด','2'=>'เข็ม','3'=>'ครั้ง','4'=>'ซอง','5'=>'แผง','6'=>'เม็ด','7'=>'หลอด','8'=>'อัน'],null,'sm-9',['placeholder'=>'ทั้งหมด']) }}
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="form-material">
                                {{ Form::radio('stk_balance_radio', '0', true) }} มากกว่า &emsp;                                                               
                                {{ Form::radio('stk_balance_radio', '1') }} น้อยกว่า
                                {{ Form::number('stk_balance','0',['placeholder'=>'กรุณาระบุจำนวนคงเหลือ','style'=>'text-align:right;']) }}
                                <label for="stk_balance_radio">จำนวนคงเหลือ</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="ค้นหา"><i class="fa fa-search"></i></button>
                </div>
            </div>
            {!! Form::close() !!}
            <br>
            <br>
        </div>
    </div>
    <!-- END Floating Labels -->

    <!-- Start table -->
    <div class="block block-bordered">
        <div class="block-header">
            <h2 class="block-title">รายการยา</h2>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;"></th>
                        <th style="text-align: center;width: 30%">ชื่อยา</th>
                        <th style="text-align: center;">ชื่อเรียกยาเพิ่มเติม</th>
                        <th style="text-align: center;width: 15%">หน่วย</th>
                        <th style="text-align: center;width: 10%">จำนวนคงเหลือ</th>
                        <th style="text-align: center;width: 10%">ราคาทุน</th>
                        <th style="text-align: center;width: 10%">ราคาขาย</th>
   
                    </tr>
                </thead>
                @foreach ($items as $key => $item)
                <tr>
                    <td>
                        <a href="{{ route('item.show',$item->itm_id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="ดูข้อมูลยา"><i class="fa fa-address-card-o"></i></a>  
                    </td>
                    <td style="vertical-align: middle;">{{ $item->itm_name }}</td>
                    <td style="vertical-align: middle;">{{ $item->itm_name2 }}</td>
                    <td style="vertical-align: middle;">{{ $item->itm_typeName }}</td>
                    <td style="vertical-align: middle;text-align: right;">{{ $item->stk_balance }}</td>
                    <td style="vertical-align: middle;text-align: right;">{{ $item->stk_cost }}</td>
                    <td style="vertical-align: middle;text-align: right;">{{ $item->stk_nsp }}</td>
                </tr>
                @endforeach
            </table>
              {!! $items->render() !!}
        </div>
    </div>
    <!-- end table -->
</div>
<!-- END Page Content -->
@endsection