<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <div class="col-{{ $col }}">
        <div class="form-material">
            {{ Form::date($name, $value, array_merge(['class' => 'form-control','id' => $name], $attributes)) }}
            {{--  <input class="form-control" type="text" id="material-text2" name="material-text2">  --}}
            <label for="material-text2">{{ $label }}</label>
        </div>
        @if ($errors->has($name))
            <div class="help-block animated fadeInDown">{{ $errors->first($name) }}</div>
        @endif
    </div>
</div>