<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}" style="width: 100%;">
    <div class="col-{{ $col }}">
        <div class="form-material">
            {{ Form::checkbox($name, $value,$checked,array_merge(['class' => '','id' => $name], $attributes)) }}
            {{--  <input class="form-control" type="text" id="material-text2" name="material-text2">  --}}
            <label for="$name">{{ $label }}</label>
        </div>
        @if ($errors->has($name))
            <div class="help-block animated fadeInDown">{{ $errors->first($name) }}</div>
        @endif
    </div>
</div>
