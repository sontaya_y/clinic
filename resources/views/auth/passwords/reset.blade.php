@extends('layouts.template')

@section('content')
<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                <i class="si si-key"></i> เปลี่ยนรหัสผ่าน
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>ผู้ใช้</li>
                <li>
                    <a class="link-effect" href="">เปลี่ยนรหัสผ่าน</a>
                </li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">

    {{ Form::open(['action'=>'Auth\ResetPasswordController@reset','Method'=>'POST','class'=>'form-horizontal push-10-t']) }}
        <div class="col-sm-12">
            {{ Form::inpPassword('รหัสผ่านใหม่','password','','sm-3',['placeholder'=>'กรุณาระบุรหัสผ่านใหม่','autofocus'=>'']) }}
        </div>
        <div class="col-sm-12">
            {{ Form::inpPassword('รหัสผ่านใหม่อีกรอบ','password_confirmation','','sm-3',['placeholder'=>'กรุณาระบุรหัสผ่านใหม่','autofocus'=>'']) }}
        </div>
        <div class="col-sm-12">
            <input type="hidden" name="username" value="{{ Auth::user()->username }}">
            <input type="hidden" name="token" value="{{ Auth::user()->remember_token }}">
            <button type="submit" class="btn btn-primary" style="" ><i class="fa fa-save"></i> บันทึก</button>
        </div>
    {{ Form::close() }}

</div>
<!-- END Page Content -->
@endsection
