@extends('layouts.template')
@section('content')
<!-- Page Content -->
<div class="content">
    <div class="block block-bordered">
        
        <div class="block-content">
            <div class="row">
                <!-- Form Add -->
                <div class="col-md-6">

                    <h4>เพิ่มผู้ใช้งาน</h4>
                    <hr>
                    {{ Form::open(['route'=>'user.store','method'=>'POST','class'=>'form-horizontal','style'=>'padding:20px;']) }}
                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::inpText('Username','username','','sm-6',['placeholder'=>'กรุณาระบุ Username','autofocus'=>'']) }}
                        </div>
                        <div class="col-sm-12">
                            {{ Form::inpPassword('รหัสผ่านใหม่','password','','sm-6',['placeholder'=>'กรุณาระบุรหัสผ่านใหม่','autofocus'=>'']) }}
                        </div>
                        <div class="col-sm-12">
                            {{ Form::inpPassword('รหัสผ่านใหม่อีกรอบ','password_confirmation','','sm-6',['placeholder'=>'กรุณาระบุรหัสผ่านใหม่','autofocus'=>'']) }}
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary" style="" ><i class="fa fa-plus"></i> เพิ่ม</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <!-- User List -->
                <div class="col-md-6">
                    <h4>รายชื่อผู้ใช้งาน</h4>
                    <hr>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 40px;"></th>
                                <th>Username</th>
                            </tr>
                        </thead>
                        @foreach ($users as $key => $user)
                        <tr>
                            <td>
                                <a href="javascript:del({{$user->usr_id}})" class="btn btn-sm btn-danger" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></a>  
                                {!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->usr_id],'style'=>'display:none','id'=>'delForm'.$user->usr_id]) !!}
                                {!! Form::submit('del', ['style' => 'display:none;']) !!}
                                {!! Form::close() !!}
                            </td>
                            <td style="vertical-align: middle;">{{ $user->username }}</td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $users->appends(Request::input())->links() }}
                    
                </div>
            </div>
        </div>

    </div>

</div>
<!-- END Page Content -->
@endsection

@section('js') 
<script>
    function del(id){
        swal({
            title: 'ลบข้อมูลผู้ใช้งานคนนี้?',
            text: 'เมื่อลบแล้วจะไม่สามารถกู้คืนได้ คุณแน่ใจหรือไม่!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'ใช่, ฉันต้องการลบ!',
            cancelButtonText: 'ไม่',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 50);
                });
            }
        }).then(
            function (result) {
                $('#delForm'+id).submit()
            }, function(dismiss) {
                
            }
        );
    }
</script>
@endsection