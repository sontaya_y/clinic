<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'username หรือ password ไม่ถูกต้อง.',
    'throttle' => 'ท่านระบุ username หรือ password. กรุณาลองใหม่ในอีก :seconds วินาที.',

];
