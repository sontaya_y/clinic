<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->route('home');
});
Route::get('/home', 'HomeController@index')->name('home');

/*    
 *   Patient & Medicalrecords
*/
Route::resource('/patient', 'PatientController')->middleware('auth');
Route::get('/patientlist', 'PatientController@list')->middleware('auth')->name('patientlist');
Route::post('/medicalrecords', 'MedicalrecordsController@store')->middleware('auth');
Route::delete('/medicalrecords', 'MedicalrecordsController@destroy')->middleware('auth');
Route::PUT('/medicalrecords', 'MedicalrecordsController@update')->middleware('auth');

/*    
 *   User Managment
*/
Route::resource('/user', 'UserController')->middleware('auth');

Route::resource('/item', 'ItemController');

Route::get('/checkout', 'ItemCheckoutcontroller@index')->name('checkout');
Route::post('/checkout', 'ItemCheckoutcontroller@checkout');
Route::get('/itemlist', 'ItemCheckoutcontroller@list')->name('itemlist');
Route::get('/getitem/{id}', 'ItemCheckoutcontroller@getItem')->name('getitem');
Route::get('/itemstock/{id}', 'ItemCheckoutcontroller@stock')->name('itemstock');
Route::get('/print', 'ItemCheckoutcontroller@print')->name('print');