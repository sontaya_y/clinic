# README #

### Dev Tools ###

* Xampp (PHP7) => https://www.apachefriends.org
* Comporser => https://getcomposer.org
* Visual Studio Code => https://code.visualstudio.com

### Visual Studio Code Extensions ###

* Laravel Artisan
* Laravel Blade Snippets
* Laravel 5 Snippets v.1.3*

### Setup Project ###

1. open CMD in project
2. run "composer update"
3. run "php artisan migrate --seed"

### Start Server ###

1. open CMD in project
2. run "php artisan serve"

### How to reset Database ###

1. [CMD] "php artisan migrate:reset"
2. [CMD] "php artisan migrate --seed" 

### Html Form Helper ###

* Form => https://laravelcollective.com/docs/5.4/html